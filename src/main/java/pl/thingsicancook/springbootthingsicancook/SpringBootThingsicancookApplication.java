package pl.thingsicancook.springbootthingsicancook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootThingsicancookApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootThingsicancookApplication.class, args);
	}

}
